# Licenses

This directory contains files from third parties.

- `nginx.conf` is based on [docker-nginx-rtmp](https://github.com/alfg/docker-nginx-rtmp), MIT License
- `player.html` is based on [docker-nginx-rtmp](https://github.com/alfg/docker-nginx-rtmp), MIT License
- `static/hls.min.js` is from [hls.js](https://github.com/video-dev/hls.js), Apache 2.0 License
- `inc/fsfe_logo.png` is the property of the [FSFE](https://fsfe.org/about/graphics/), compare webpage for licensing information
